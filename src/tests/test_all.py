import os
import requests
import json

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

# About: тестирование происходит последовательно для проверки всех работающих эндпоинтов
# для этого где-то будем добавлять статьи, где-то удалять, последовательность описана
# ниже перед каждой тестирующей функцией

TEST_ARTICLE_IDS = [] # массив для хранения id статей, которые вернет API
UNKNOWN_ID = '00000000-0000-0000-0000-000000000000' # uuid type

TEST_ARTICLE_1 = {
    'keywords': ['test', 'test2'],
    'title': 'test',
    'text': 'test',
    'date': 1593723037,
    'source': 'GITHUB',
    'link': 'https://test.com',
    'image': 'https://test.com',
}

TEST_ARTICLE_2 = {
    'keywords': ['space', 'NASA'],
    'title': 'NASA',
    'text': 'The National Aeronautics and Space Administration \
    (NASA; /ˈnæsə/) is an independent agency of the U.S. federal \
    government responsible for the civilian space program, as well \
    as aeronautics and space research',
    'date': 1593723037,
    'source': 'wikipedia',
    'link': 'https://en.wikipedia.org/wiki/NASA',
    'image': 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/NASA_seal.svg/300px-NASA_seal.svg.png'
}

TEST_ARTICLE_3 = {
    'keywords': ['cyberpunk'],
    'title': 'Весь мир (через боль) играет в Cyberpunk 2077. Там очень много багов — Киану Ривз курит летающую \
    сигарету, повсюду растут крошечные деревья, а люди теряют штаны',
    'text': '10 декабря вышла самая ожидаемая игра 2020 года — Cyberpunk 2077 от создателей «Ведьмака», польской \
    студии CD Projekt Red. Казалось, что это последний шанс хоть немного спасти репутацию 2020-го, но все снова \
    пошло не так.',
    'date': 1593723065,
    'source': 'meduza',
    'link': 'https://meduza.io/shapito/2020/12/12/ves-mir-cherez-bol-igraet-v-cyberpunk-2077-tam-ochen-mnogo-bagov-kianu-rivz-kurit-letayuschuyu-sigaretu-povsyudu-rastut-kroshechnye-derevya-a-lyudi-teryayut-shtany-i-teleportiruyutsya',
    'image': 'https://meduza.io/impro/O_8gFyThzcR9Xex10CJA8tk0hEd0uix4tDYYesZZz4M/fill/650/0/ce/1/aHR0cHM6Ly9tZWR1/emEuaW8vaW1hZ2Uv/YXR0YWNobWVudHMv/aW1hZ2VzLzAwNi8y/NTIvNzM2L29yaWdp/bmFsLzVkN0NKVi1l/LVBXa1dPdWVDOVVx/aXcuanBn.webp',
}

# тестируем получение всех статей, ответ - пустой объект
def test_get_articles():
    response = requests.get(URL + '/articles')
    assert response.status_code == 200
    body = response.json()
    assert 'articles' in body
    assert len(body['articles']) == 0

# тестируем добавление статьи
def test_post_article():
    response = requests.post(URL + '/articles', data=TEST_ARTICLE_1)
    assert response.status_code == 201
    body = response.json()
    id = body['_id']
    TEST_ARTICLE_IDS.append(id)
    body.pop('_id') # удаляем id из ответа чтобы сравнить с данными первоначальной
    assert body == TEST_ARTICLE_1

# тестируем добавление еще одной статьи
def test_post_one_more_article():
    response = requests.post(URL + '/articles', data=TEST_ARTICLE_2)
    assert response.status_code == 201
    body = response.json()
    id = body['_id']
    TEST_ARTICLE_IDS.append(id)
    body.pop('_id')
    assert body == TEST_ARTICLE_2

# тестируем добавление статьи с одним из некорректных полей
def test_post_article_with_incorrect_fields():
    TEST_ARTICLE_2['link'] = 'https://'# Неправильная ссылка link
    response = requests.post(URL + '/articles', data=TEST_ARTICLE_2)
    TEST_ARTICLE_2['link'] = 'https://en.wikipedia.org/wiki/NASA' # возвращаем корректные данные
    assert response.status_code == 400
    error = response.json()
    assert error['error'] == 'Bad Request'
    assert error['message'] == 'Инвалидный url ссылки'

# тестируем получение первой статьи по ее id
def test_get_article_first():
    response = requests.get(URL + '/articles/' + TEST_ARTICLE_IDS[0])
    assert response.status_code == 200
    body = response.json()
    assert TEST_ARTICLE_IDS[0] == body['_id']
    body.pop('_id')
    assert body == TEST_ARTICLE_1

# тестируем получение второй статьи по ее id
def test_get_article_second():
    response = requests.get(URL + '/articles/' + TEST_ARTICLE_IDS[1])
    assert response.status_code == 200
    body = response.json()
    assert TEST_ARTICLE_IDS[1] == body['_id']
    body.pop('_id')
    assert body == TEST_ARTICLE_2

# тестируем получение статьи по несуществующему id
def test_get_not_found_article():
    response = requests.get(URL + '/articles/' + UNKNOWN_ID)
    assert response.status_code == 404
    error = response.json()
    assert error['message'] == 'Запрашиваемый объект не найден'

# тестируем получение статьи по невалидному id
def test_get_invalid_id_article():
    response = requests.get(URL + '/articles/' + 'KICK_YOUR_A$$_BABY')
    assert response.status_code == 400
    error = response.json()
    assert error['error'] == 'Bad Request'
    assert error['message'] == 'Некорректный id статьи'

# тестируем получение всех статей после 2ух добавлений, ответ - список из двух статей
def test_get_all_two_articles():
    response = requests.get(URL + '/articles')
    assert response.status_code == 200
    body = response.json()
    assert 'articles' in body
    assert len(body['articles']) == 2
    assert TEST_ARTICLE_IDS[0] == body['articles'][0]['_id']
    assert TEST_ARTICLE_IDS[1] == body['articles'][1]['_id']
    body['articles'][0].pop('_id')
    body['articles'][1].pop('_id')
    assert TEST_ARTICLE_1 == body['articles'][0]
    assert TEST_ARTICLE_2 == body['articles'][1]

# тестируем обновление существующих атрибутов второй статьи
def test_put_article():
    keys = {
        'title': 'ROSSPACE IS COOLER THAN NASA',
        'source': 'Slava said so!'
    }
    response = requests.put(URL + '/articles/' + TEST_ARTICLE_IDS[1], data=keys)
    assert response.status_code == 200
    body = response.json()
    assert body['_id'] == TEST_ARTICLE_IDS[1]
    assert body['title'] == keys['title']
    assert body['source'] == keys['source']

# тестируем несуществующие атрибуты второй статьи
def test_put_unknown_keys_article():
    keys = {
        'tile': 'There is a mistake in key title - TITLE NOT TILE',
    }
    response = requests.put(URL + '/articles/' + TEST_ARTICLE_IDS[1], data=keys)
    assert response.status_code == 400
    error = response.json()
    assert error['error'] == 'Bad Request'
    assert error['message'] == '\"tile\" is not allowed'
    assert error['validation']['keys'][0] == 'tile'

# тестируем обновление статьи с несуществующим id
def test_put_not_found_article():
    keys = {
        'keywords': ['OK', 'OK2'],
    }
    response = requests.put(URL + '/articles/' + UNKNOWN_ID, data=keys)
    assert response.status_code == 404
    error = response.json()
    assert error['message'] == 'Запрашиваемый объект не найден'

# тестирум удаление статьи с несуществующим id
def test_delete_not_found_article():
    response = requests.delete(URL + '/articles/' + UNKNOWN_ID)
    assert response.status_code == 404
    error = response.json()
    assert error['message'] == 'Запрашиваемый объект не найден'

# тестируем удаление второй статьи по ее id
def test_delete_second_article():
    response = requests.delete(URL + '/articles/' + TEST_ARTICLE_IDS[1])
    assert response.status_code == 200
    body = response.json()
    assert body['message'] == 'Статья успешно удалена'

# тестируем получение всех статей после 1го удаления, ответ - список из одной статьи
def test_get_after_delete():
    response = requests.get(URL + '/articles')
    assert response.status_code == 200
    body = response.json()
    assert 'articles' in body
    assert len(body['articles']) == 1
    assert TEST_ARTICLE_IDS[0] == body['articles'][0]['_id']
    body['articles'][0].pop('_id')
    assert TEST_ARTICLE_1 == body['articles'][0]

# тестируем удаление первой статье по ее id
def test_delete_first_article():
    response = requests.delete(URL + '/articles/' + TEST_ARTICLE_IDS[0])
    assert response.status_code == 200
    body = response.json()
    assert body['message'] == 'Статья успешно удалена'

# тестируем получение всех статей после 2го удаления, ответ - пустой объект
def test_get_after_delete():
    response = requests.get(URL + '/articles')
    assert response.status_code == 200
    body = response.json()
    assert 'articles' in body
    assert len(body['articles']) == 0

# тестируем запрос на несуществующий метод
def test_unknown_method():
    response = requests.get(URL + '/wannasleep')
    assert response.status_code == 404
    error = response.json()
    assert error['message'] == 'Запрашиваемый ресурс не найден'
