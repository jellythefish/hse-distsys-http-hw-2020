const { Joi } = require('celebrate');
const urlRegex = require('../constants/regexWeburl');
const uuidRegex = require('../constants/regexUuid');
const BadRequestError = require('../helpers/errors/BadRequestError');
const { INVALID_LINK_URL, INVALID_IMAGE_URL, INVALID_DATE, INVALID_SOURCE, INVALID_KEYWORDS,
INVALID_ARTICLE_ID, INVALID_TITLE, INVALID_TEXT } = require('../constants/errors');

const getArticleSchema = {
  params: Joi.object().keys({
    articleId: Joi.string().regex(uuidRegex).error(new BadRequestError(INVALID_ARTICLE_ID)),
  }),
}

const createArticleSchema = {
  body: Joi.object().keys({
    keywords: Joi.array().items(Joi.string().required().max(30).error(new BadRequestError(INVALID_KEYWORDS))),
    title: Joi.string().required().max(150).error(new BadRequestError(INVALID_TITLE)),
    text: Joi.string().required().error(new BadRequestError(INVALID_TEXT)),
    date: Joi.number().integer().min(0).required().error(new BadRequestError(INVALID_DATE)),
    source: Joi.string().required().max(30).error(new BadRequestError(INVALID_SOURCE)),
    link: Joi.string().required().regex(urlRegex).error(new BadRequestError(INVALID_LINK_URL)),
    image: Joi.string().required().regex(urlRegex).error(new BadRequestError(INVALID_IMAGE_URL)),
  }),
};

const putArticleSchema = {
  body: Joi.object().keys({
    keywords: Joi.array().items(Joi.string().max(30).error(new BadRequestError(INVALID_KEYWORDS))),
    title: Joi.string().max(150).error(new BadRequestError(INVALID_TITLE)),
    text: Joi.string().error(new BadRequestError(INVALID_TEXT)),
    date: Joi.number().integer().min(0).error(new BadRequestError(INVALID_DATE)),
    source: Joi.string().max(30).error(new BadRequestError(INVALID_SOURCE)),
    link: Joi.string().regex(urlRegex).error(new BadRequestError(INVALID_LINK_URL)),
    image: Joi.string().regex(urlRegex).error(new BadRequestError(INVALID_IMAGE_URL)),
  }),
}

const deleteArticleSchema = {
  params: Joi.object().keys({
    articleId: Joi.string().regex(uuidRegex).error(new BadRequestError(INVALID_ARTICLE_ID)),
  }),
};

module.exports = { getArticleSchema, createArticleSchema, putArticleSchema, deleteArticleSchema };
