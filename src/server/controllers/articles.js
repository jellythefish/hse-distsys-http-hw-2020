const uuid = require('uuid');
const NotFoundError = require('../helpers/errors/NotFoundError');
const { OBJECT_NOT_FOUND } = require('../constants/errors');
const { SUCCESSFULLY_DELETED } = require('../constants/messages');

const articles = []

const getArticles = (req, res, next) => {
  res.send({articles});
};

const getArticle = (req, res, next) => {
  const { articleId } = req.params;
  const article = articles.find(article => article._id == articleId)
  if (!article) {
    next(new NotFoundError(OBJECT_NOT_FOUND))
  } else {
    res.send(article);
  }
}

const createArticle = (req, res, next) => {
  const { keywords, title, text, date, source, link, image } = req.body;
  const _id = uuid.v4();
  const article = { _id, keywords, title, text, date, source, link, image };
  articles.push(article)
  res.status(201).send(article)  
};

const putArticle = (req, res, next) => {
  const { articleId } = req.params;
  const articleIndex = articles.findIndex(article => article._id == articleId)
  if (articleIndex == -1) {
    next(new NotFoundError(OBJECT_NOT_FOUND))
  } else {
    for (const [key, value] of Object.entries(req.body)) {
      articles[articleIndex][key] = value;
    }
    res.send(articles[articleIndex]);
  }
}

const deleteArticle = (req, res, next) => {
  const { articleId } = req.params;
  const articleIndex = articles.findIndex(article => article._id == articleId)
  if (articleIndex == -1) {
    next(new NotFoundError(OBJECT_NOT_FOUND))
  } else {
    articles.splice(articleIndex, 1);
    res.status(200).send({ 'message': SUCCESSFULLY_DELETED });
  }
};

module.exports = { getArticles, getArticle, putArticle, createArticle, deleteArticle };
