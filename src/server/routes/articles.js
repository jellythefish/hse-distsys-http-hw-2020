const router = require('express').Router();
const { celebrate } = require('celebrate');
const { getArticles, getArticle, createArticle, putArticle, deleteArticle } = require('../controllers/articles');
const { getArticleSchema, putArticleSchema, createArticleSchema, deleteArticleSchema } = require('../middlewares/joiSchemas');

router.post('/', celebrate(createArticleSchema), createArticle);
router.get('/', getArticles);
router.get('/:articleId', celebrate(getArticleSchema), getArticle);
router.put('/:articleId', celebrate(putArticleSchema), putArticle)
router.delete('/:articleId', celebrate(deleteArticleSchema), deleteArticle);

module.exports = router;
