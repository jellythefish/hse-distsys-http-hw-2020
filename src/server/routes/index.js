const router = require('express').Router();
const { celebrate, errors } = require('celebrate');
const articlesRouter = require('./articles');
const { requestLogger, errorLogger } = require('../middlewares');
const NotFoundError = require('../helpers/errors/NotFoundError');

router.use(requestLogger);
router.use('/articles', articlesRouter);
router.use(errorLogger);
router.use(errors()); // celebrate errors handler
router.use('*', (req, res, next) => next(new NotFoundError()));

module.exports = router;
