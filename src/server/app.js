const express = require('express');
const bodyParser = require('body-parser');

const { SERVER_PORT } = require('./config');
const { errorMiddleware } = require('./middlewares');
const router = require('./routes');

// launching web-server
const app = express();
app.listen(SERVER_PORT, () => {
  console.log(`App listening on port ${SERVER_PORT}`);
});

// adding middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// adding main router
app.use(router);
app.use(errorMiddleware);